var frame = require("ui/frame");
const accelerometer = require("nativescript-accelerometer");
let accelerometerListening = false;
var segmentedBarModule = require("ui/segmented-bar");
let getSegmentedBarItems = function () {
  let segmentedBarItem1 = new segmentedBarModule.SegmentedBarItem();
  segmentedBarItem1.title = "Item 1";
  let segmentedBarItem2 = new segmentedBarModule.SegmentedBarItem();
  segmentedBarItem2.title = "Item 2";
  return [segmentedBarItem1, segmentedBarItem2];
}


var observableModule = require("tns-core-modules/data/observable");

function HomeViewModel() {
  var viewModel = observableModule.fromObject({
    highDataModel: [
        { Year: 2000, Amount: 15, Impact: 1 },
        { Year: 1456, Amount: 13, Impact: 7 },
        { Year: 1866, Amount: 25, Impact: 10 },
        { Year: 1900, Amount: 5, Impact: 3 },
        { Year: 1700, Amount: 17, Impact: 4 },
         { Year: 1600, Amount: 20, Impact: 1 }
    ],

    middleDataModel: [
        { Year: 1200, Amount: 15, Impact: 1 },
        { Year: 1156, Amount: 13, Impact: 7 },
        { Year: 1000, Amount: 25, Impact: 10 },
        { Year: 900, Amount: 5, Impact: 3 },
        { Year: 700, Amount: 17, Impact: 4 },
        { Year: 600, Amount: 20, Impact: 1 },
    ],

    lowDataModel: [
        { Year: 200, Amount: 15, Impact: 1 },
        { Year: 456, Amount: 13, Impact: 7 },
        { Year: 366, Amount: 25, Impact: 10 },
        { Year: 100, Amount: 5, Impact: 3 },
        { Year: 340, Amount: 17, Impact: 4 },
        { Year: 135, Amount: 20, Impact: 1 },
    ],

    gaugeValue: 2,

    mainContentText: "SideDrawer for NativeScript can be easily setup in the XML definition of your page by defining main- and drawer-content. The component"
    + " has a default transition and position and also exposes notifications related to changes in its state. Swipe from left to open side drawer.",
    onOpenDrawerTap: function () {
        var sideDrawer = frame.topmost().getViewById("sideDrawer");
        sideDrawer.showDrawer();
    },
    onCloseDrawerTap: function () {
        var sideDrawer = frame.topmost().getViewById("sideDrawer");
        sideDrawer.closeDrawer();
    },

    accelerometerValues: null,

    startAccelerometer: function () {
      if (accelerometerListening) {
        accelerometer.stopAccelerometerUpdates();
      }
      accelerometer.startAccelerometerUpdates(data => {
        accelerometerListening = true;
        this.accelerometerValues = data;
      }, {
          sensorDelay: "ui"
        });
    },

    isBusy: true,

    searchPhrase: "",
    onSearchSubmit: function(args) {
      let searchBar = args.object;
      console.log("You are searching for " + searchBar.text);
    },

    areaSource: [
      { Category: "Mar", Amount: 51 },
      { Category: "Apr", Amount: 81 },
      { Category: "May", Amount: 89 },
      { Category: "Jun", Amount: 97 }
    ],

    areaSource2: [
      { Category: "Mar", Amount: 60 },
      { Category: "Apr", Amount: 87 },
      { Category: "May", Amount: 91 },
      { Category: "Jun", Amount: 95 }
    ],

    financialSourceItems: [
        { Date: "01/6/2015", Open: 100, Close: 85, Low: 50, High: 139 },
        { Date: "27/7/2015", Open: 60, Close: 150, Low: 40, High: 159 },
        { Date: "18/8/2015", Open: 120, Close: 81, Low: 45, High: 141 },
        { Date: "19/9/2015", Open: 105, Close: 200, Low: 55, High: 250 }
    ],

    scatterSource: [
        { Age: 20, Salary: 10000, Spendings: 4500, Savings: 5500, Impact: 1 },
        { Age: 25, Salary: 12300, Spendings: 6500, Savings: 5200, Impact: 7 },
        { Age: 30, Salary: 14000, Spendings: 8500, Savings: 5500, Impact: 10 },
        { Age: 35, Salary: 18000, Spendings: 9500, Savings: 7500, Impact: 6 },
        { Age: 40, Salary: 19520, Spendings: 15540, Savings: 3800, Impact: 4 },
        { Age: 45, Salary: 20000, Spendings: 15500, Savings: 4500, Impact: 2 },
        { Age: 50, Salary: 24200, Spendings: 20500, Savings: 3700, Impact: 11 },
        { Age: 55, Salary: 24000, Spendings: 22500, Savings: 1500, Impact: 8 },
        { Age: 60, Salary: 22000, Spendings: 22500, Savings: 500, Impact: 1 },
        { Age: 65, Salary: 20000, Spendings: 20500, Savings: 10, Impact: 9 }
    ],

    categoricalSource: [
        { Country: "Germany", Amount: 15, SecondVal: 14, ThirdVal: 24 },
        { Country: "France", Amount: 13, SecondVal: 23, ThirdVal: 25 },
        { Country: "Bulgaria", Amount: 24, SecondVal: 17, ThirdVal: 23 },
        { Country: "Spain", Amount: 11, SecondVal: 19, ThirdVal: 24 },
        { Country: "USA", Amount: 18, SecondVal: 8, ThirdVal: 21 }
    ],

    pieSource: [
        { Brand: "Audi", Amount: 10 },
        { Brand: "Mercedes", Amount: 76 },
        { Brand: "Fiat", Amount: 60 },
        { Brand: "BMW", Amount: 24 },
        { Brand: "Crysler", Amount: 40 }
    ],

    segmentedBarItems: getSegmentedBarItems(),
    selectedBarIndex: 0,    onButtonTap: function () {
      console.log("Button was pressed");
    },


  });

  return viewModel;
}

module.exports = HomeViewModel;
