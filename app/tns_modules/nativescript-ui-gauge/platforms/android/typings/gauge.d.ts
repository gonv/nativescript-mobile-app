declare module com {
	export module telerik {
		export module widget {
			export module gauge {
				export class BuildConfig extends java.lang.Object {
					public static class: java.lang.Class<com.telerik.widget.gauge.BuildConfig>;
					public static DEBUG: boolean;
					public static APPLICATION_ID: string;
					public static BUILD_TYPE: string;
					public static FLAVOR: string;
					public static VERSION_CODE: number;
					public static VERSION_NAME: string;
					public constructor();
				}
			}
		}
	}
}

declare module com {
	export module telerik {
		export module widget {
			export module gauge {
				export abstract class RadGaugeView extends globalAndroid.view.ViewGroup {
					public static class: java.lang.Class<com.telerik.widget.gauge.RadGaugeView>;
					public scales: java.util.ArrayList<com.telerik.widget.scales.GaugeScale>;
					public title: globalAndroid.widget.TextView;
					public subtitle: globalAndroid.widget.TextView;
					public titleHorizontalOffset: number;
					public titleVerticalOffset: number;
					public subtitleHorizontalOffset: number;
					public subtitleVerticalOffset: number;
					public fillColor: number;
					public fillPaint: globalAndroid.graphics.Paint;
					public getTitle(): globalAndroid.widget.TextView;
					public invalidateChildInParent(param0: native.Array<number>, param1: globalAndroid.graphics.Rect): globalAndroid.view.ViewParent;
					public focusSearch(param0: number): globalAndroid.view.View;
					public setFillPaint(param0: globalAndroid.graphics.Paint): void;
					public focusableViewAvailable(param0: globalAndroid.view.View): void;
					public createContextMenu(param0: globalAndroid.view.ContextMenu): void;
					public isLayoutRequested(): boolean;
					public removeScale(param0: number): void;
					public setSubtitle(param0: globalAndroid.widget.TextView): void;
					public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.util.AttributeSet);
					public addView(param0: globalAndroid.view.View, param1: number, param2: number): void;
					public removeAllScales(): void;
					public onKeyUp(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public addScale(param0: number, param1: com.telerik.widget.scales.GaugeScale): void;
					public addView(param0: globalAndroid.view.View): void;
					public addView(param0: globalAndroid.view.View, param1: number, param2: globalAndroid.view.ViewGroup.LayoutParams): void;
					public focusSearch(param0: globalAndroid.view.View, param1: number): globalAndroid.view.View;
					public recomputeViewAttributes(param0: globalAndroid.view.View): void;
					public clearChildFocus(param0: globalAndroid.view.View): void;
					public onDraw(param0: globalAndroid.graphics.Canvas): void;
					public addScale(param0: com.telerik.widget.scales.GaugeScale): void;
					public initWithXml(param0: globalAndroid.content.res.TypedArray): void;
					public getChildVisibleRect(param0: globalAndroid.view.View, param1: globalAndroid.graphics.Rect, param2: globalAndroid.graphics.Point): boolean;
					public bringChildToFront(param0: globalAndroid.view.View): void;
					public unscheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable, param1: java.lang.Runnable): void;
					public getScales(): java.util.ArrayList<com.telerik.widget.scales.GaugeScale>;
					public requestTransparentRegion(param0: globalAndroid.view.View): void;
					public onKeyLongPress(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public setTitle(param0: globalAndroid.widget.TextView): void;
					public childDrawableStateChanged(param0: globalAndroid.view.View): void;
					public scheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable, param1: java.lang.Runnable, param2: number): void;
					public requestFitSystemWindows(): void;
					public removeScale(param0: com.telerik.widget.scales.GaugeScale): void;
					public getParent(): globalAndroid.view.ViewParent;
					public startActionModeForChild(param0: globalAndroid.view.View, param1: globalAndroid.view.ActionMode.Callback): globalAndroid.view.ActionMode;
					public getFillPaint(): globalAndroid.graphics.Paint;
					public getSubtitleHorizontalOffset(): number;
					public requestArrange(): void;
					public updateViewLayout(param0: globalAndroid.view.View, param1: globalAndroid.view.ViewGroup.LayoutParams): void;
					public requestChildFocus(param0: globalAndroid.view.View, param1: globalAndroid.view.View): void;
					public getTitleHorizontalOffset(): number;
					public requestDisallowInterceptTouchEvent(param0: boolean): void;
					public addView(param0: globalAndroid.view.View, param1: number): void;
					public unscheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable): void;
					public addView(param0: globalAndroid.view.View, param1: globalAndroid.view.ViewGroup.LayoutParams): void;
					public invalidateChild(param0: globalAndroid.view.View, param1: globalAndroid.graphics.Rect): void;
					public sendAccessibilityEvent(param0: number): void;
					public animateGauge(): void;
					public requestLayout(): void;
					public requestSendAccessibilityEvent(param0: globalAndroid.view.View, param1: globalAndroid.view.accessibility.AccessibilityEvent): boolean;
					public getParentForAccessibility(): globalAndroid.view.ViewParent;
					public onKeyDown(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public setTitleVerticalOffset(param0: number): void;
					public getSubtitle(): globalAndroid.widget.TextView;
					public setTitleHorizontalOffset(param0: number): void;
					public requestRender(): void;
					public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.util.AttributeSet, param2: number);
					public init(): void;
					public showContextMenuForChild(param0: globalAndroid.view.View): boolean;
					public sendAccessibilityEventUnchecked(param0: globalAndroid.view.accessibility.AccessibilityEvent): void;
					public setSubtitleHorizontalOffset(param0: number): void;
					public onKeyMultiple(param0: number, param1: number, param2: globalAndroid.view.KeyEvent): boolean;
					public removeView(param0: globalAndroid.view.View): void;
					public constructor(param0: globalAndroid.content.Context);
					public setSubtitleVerticalOffset(param0: number): void;
					public setScales(param0: java.util.ArrayList<com.telerik.widget.scales.GaugeScale>): void;
					public onLayout(param0: boolean, param1: number, param2: number, param3: number, param4: number): void;
					public invalidateDrawable(param0: globalAndroid.graphics.drawable.Drawable): void;
					public getSubtitleVerticalOffset(): number;
					public getFillColor(): number;
					public setFillColor(param0: number): void;
					public requestChildRectangleOnScreen(param0: globalAndroid.view.View, param1: globalAndroid.graphics.Rect, param2: boolean): boolean;
					public getTitleVerticalOffset(): number;
				}
			}
		}
	}
}

declare module com {
	export module telerik {
		export module widget {
			export module gauge {
				export class RadRadialGaugeView extends com.telerik.widget.gauge.RadGaugeView {
					public static class: java.lang.Class<com.telerik.widget.gauge.RadRadialGaugeView>;
					public invalidateChildInParent(param0: native.Array<number>, param1: globalAndroid.graphics.Rect): globalAndroid.view.ViewParent;
					public getParent(): globalAndroid.view.ViewParent;
					public focusSearch(param0: number): globalAndroid.view.View;
					public startActionModeForChild(param0: globalAndroid.view.View, param1: globalAndroid.view.ActionMode.Callback): globalAndroid.view.ActionMode;
					public focusableViewAvailable(param0: globalAndroid.view.View): void;
					public createContextMenu(param0: globalAndroid.view.ContextMenu): void;
					public isLayoutRequested(): boolean;
					public updateViewLayout(param0: globalAndroid.view.View, param1: globalAndroid.view.ViewGroup.LayoutParams): void;
					public requestChildFocus(param0: globalAndroid.view.View, param1: globalAndroid.view.View): void;
					public requestDisallowInterceptTouchEvent(param0: boolean): void;
					public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.util.AttributeSet);
					public addView(param0: globalAndroid.view.View, param1: number, param2: number): void;
					public addView(param0: globalAndroid.view.View, param1: number): void;
					public onKeyUp(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public unscheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable): void;
					public addView(param0: globalAndroid.view.View): void;
					public addView(param0: globalAndroid.view.View, param1: globalAndroid.view.ViewGroup.LayoutParams): void;
					public invalidateChild(param0: globalAndroid.view.View, param1: globalAndroid.graphics.Rect): void;
					public addView(param0: globalAndroid.view.View, param1: number, param2: globalAndroid.view.ViewGroup.LayoutParams): void;
					public focusSearch(param0: globalAndroid.view.View, param1: number): globalAndroid.view.View;
					public sendAccessibilityEvent(param0: number): void;
					public requestLayout(): void;
					public recomputeViewAttributes(param0: globalAndroid.view.View): void;
					public clearChildFocus(param0: globalAndroid.view.View): void;
					public requestSendAccessibilityEvent(param0: globalAndroid.view.View, param1: globalAndroid.view.accessibility.AccessibilityEvent): boolean;
					public getParentForAccessibility(): globalAndroid.view.ViewParent;
					public onKeyDown(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public getChildVisibleRect(param0: globalAndroid.view.View, param1: globalAndroid.graphics.Rect, param2: globalAndroid.graphics.Point): boolean;
					public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.util.AttributeSet, param2: number);
					public showContextMenuForChild(param0: globalAndroid.view.View): boolean;
					public sendAccessibilityEventUnchecked(param0: globalAndroid.view.accessibility.AccessibilityEvent): void;
					public onKeyMultiple(param0: number, param1: number, param2: globalAndroid.view.KeyEvent): boolean;
					public removeView(param0: globalAndroid.view.View): void;
					public constructor(param0: globalAndroid.content.Context);
					public bringChildToFront(param0: globalAndroid.view.View): void;
					public unscheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable, param1: java.lang.Runnable): void;
					public requestTransparentRegion(param0: globalAndroid.view.View): void;
					public onKeyLongPress(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public onLayout(param0: boolean, param1: number, param2: number, param3: number, param4: number): void;
					public childDrawableStateChanged(param0: globalAndroid.view.View): void;
					public invalidateDrawable(param0: globalAndroid.graphics.drawable.Drawable): void;
					public scheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable, param1: java.lang.Runnable, param2: number): void;
					public requestFitSystemWindows(): void;
					public requestChildRectangleOnScreen(param0: globalAndroid.view.View, param1: globalAndroid.graphics.Rect, param2: boolean): boolean;
				}
			}
		}
	}
}

declare module com {
	export module telerik {
		export module widget {
			export module indicators {
				export abstract class GaugeBarIndicator extends com.telerik.widget.indicators.GaugeIndicator {
					public static class: java.lang.Class<com.telerik.widget.indicators.GaugeBarIndicator>;
					public barWidth: number;
					public minimum: number;
					public maximum: number;
					public location: number;
					public animationStartValue: number;
					public drawMaximum: number;
					public cap: com.telerik.widget.indicators.GaugeBarIndicatorCapMode;
					public getBarWidth(): number;
					public setRange(param0: number, param1: number): void;
					public getCap(): com.telerik.widget.indicators.GaugeBarIndicatorCapMode;
					public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.util.AttributeSet);
					public setBarWidth(param0: number): void;
					public startAnimation(param0: globalAndroid.view.animation.Animation): void;
					public getAnimationStartValue(): number;
					public onKeyUp(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public unscheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable): void;
					public sendAccessibilityEvent(param0: number): void;
					public getLocation(): number;
					public onKeyDown(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public getMinimum(): number;
					public setupValueAnimator(): void;
					public setMinimum(param0: number): void;
					public setLocation(param0: number): void;
					public startAnimation(): void;
					public initWithXml(param0: globalAndroid.content.res.TypedArray): void;
					public setCap(param0: com.telerik.widget.indicators.GaugeBarIndicatorCapMode): void;
					public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.util.AttributeSet, param2: number);
					public init(): void;
					public sendAccessibilityEventUnchecked(param0: globalAndroid.view.accessibility.AccessibilityEvent): void;
					public onKeyMultiple(param0: number, param1: number, param2: globalAndroid.view.KeyEvent): boolean;
					public setMaximum(param0: number): void;
					public constructor(param0: globalAndroid.content.Context);
					public unscheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable, param1: java.lang.Runnable): void;
					public getMaximum(): number;
					public onRestoreInstanceState(param0: globalAndroid.os.Parcelable): void;
					public onKeyLongPress(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public setAnimationStartValue(param0: number): void;
					public onSaveInstanceState(): globalAndroid.os.Parcelable;
					public invalidateDrawable(param0: globalAndroid.graphics.drawable.Drawable): void;
					public scheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable, param1: java.lang.Runnable, param2: number): void;
				}
				export module GaugeBarIndicator {
					export class SavedState extends globalAndroid.view.View.BaseSavedState {
						public static class: java.lang.Class<com.telerik.widget.indicators.GaugeBarIndicator.SavedState>;
						public static CREATOR: globalAndroid.os.Parcelable.Creator<com.telerik.widget.indicators.GaugeBarIndicator.SavedState>;
						public describeContents(): number;
						public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
					}
				}
			}
		}
	}
}

declare module com {
	export module telerik {
		export module widget {
			export module indicators {
				export class GaugeBarIndicatorCapMode {
					public static class: java.lang.Class<com.telerik.widget.indicators.GaugeBarIndicatorCapMode>;
					public static ROUND: com.telerik.widget.indicators.GaugeBarIndicatorCapMode;
					public static EDGE: com.telerik.widget.indicators.GaugeBarIndicatorCapMode;
					public static valueOf(param0: string): com.telerik.widget.indicators.GaugeBarIndicatorCapMode;
					public static values(): native.Array<com.telerik.widget.indicators.GaugeBarIndicatorCapMode>;
					public static valueOf(param0: java.lang.Class<any>, param1: string): java.lang.Enum<any>;
				}
			}
		}
	}
}

declare module com {
	export module telerik {
		export module widget {
			export module indicators {
				export abstract class GaugeIndicator extends globalAndroid.view.View {
					public static class: java.lang.Class<com.telerik.widget.indicators.GaugeIndicator>;
					public static DEFAULT_GAUGE_INDICATOR_COLOR: number;
					public scale: com.telerik.widget.scales.GaugeScale;
					public strokePaint: globalAndroid.graphics.Paint;
					public strokeColor: number;
					public strokeWidth: number;
					public fillPaint: globalAndroid.graphics.Paint;
					public fillColor: number;
					public animated: boolean;
					public animationDuration: number;
					public animator: globalAndroid.animation.ValueAnimator;
					public interpolator: globalAndroid.animation.TimeInterpolator;
					public renderSuspended: boolean;
					public onValueChangedListener: com.telerik.widget.indicators.GaugeIndicator.OnValueChangedListener;
					public setFillPaint(param0: globalAndroid.graphics.Paint): void;
					public getFillPaint(): globalAndroid.graphics.Paint;
					public getInterpolator(): globalAndroid.animation.TimeInterpolator;
					public setAnimated(param0: boolean): void;
					public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.util.AttributeSet);
					public isAnimated(): boolean;
					public getScale(): com.telerik.widget.scales.GaugeScale;
					public startAnimation(param0: globalAndroid.view.animation.Animation): void;
					public onKeyUp(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public unscheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable): void;
					public setStrokePaint(param0: globalAndroid.graphics.Paint): void;
					public getOnValueChangedListener(): com.telerik.widget.indicators.GaugeIndicator.OnValueChangedListener;
					public sendAccessibilityEvent(param0: number): void;
					public setScale(param0: com.telerik.widget.scales.GaugeScale): void;
					public getStrokeWidth(): number;
					public setInterpolator(param0: globalAndroid.animation.TimeInterpolator): void;
					public onKeyDown(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public setupValueAnimator(): void;
					public startAnimation(): void;
					public setOnValueChangedListener(param0: com.telerik.widget.indicators.GaugeIndicator.OnValueChangedListener): void;
					public initWithXml(param0: globalAndroid.content.res.TypedArray): void;
					public requestRender(): void;
					public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.util.AttributeSet, param2: number);
					public init(): void;
					public sendAccessibilityEventUnchecked(param0: globalAndroid.view.accessibility.AccessibilityEvent): void;
					public onKeyMultiple(param0: number, param1: number, param2: globalAndroid.view.KeyEvent): boolean;
					public getAnimationDuration(): number;
					public constructor(param0: globalAndroid.content.Context);
					public unscheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable, param1: java.lang.Runnable): void;
					public onKeyLongPress(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public setStrokeColor(param0: number): void;
					public invalidateDrawable(param0: globalAndroid.graphics.drawable.Drawable): void;
					public setAnimationDuration(param0: number): void;
					public scheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable, param1: java.lang.Runnable, param2: number): void;
					public getStrokeColor(): number;
					public getFillColor(): number;
					public setFillColor(param0: number): void;
					public getStrokePaint(): globalAndroid.graphics.Paint;
					public setStrokeWidth(param0: number): void;
				}
				export module GaugeIndicator {
					export class OnValueChangedListener extends java.lang.Object {
						public static class: java.lang.Class<com.telerik.widget.indicators.GaugeIndicator.OnValueChangedListener>;
						/**
						 * Constructs a new instance of the com.telerik.widget.indicators.GaugeIndicator$OnValueChangedListener interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
						 */
						public constructor(implementation: {
							valueChanged(param0: com.telerik.widget.indicators.GaugeIndicator): void;
						});
						public constructor();
						public valueChanged(param0: com.telerik.widget.indicators.GaugeIndicator): void;
					}
				}
			}
		}
	}
}

declare module com {
	export module telerik {
		export module widget {
			export module indicators {
				export class GaugeRadialBarIndicator extends com.telerik.widget.indicators.GaugeBarIndicator {
					public static class: java.lang.Class<com.telerik.widget.indicators.GaugeRadialBarIndicator>;
					public onKeyDown(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public onDraw(param0: globalAndroid.graphics.Canvas): void;
					public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.util.AttributeSet, param2: number);
					public init(): void;
					public sendAccessibilityEventUnchecked(param0: globalAndroid.view.accessibility.AccessibilityEvent): void;
					public onKeyMultiple(param0: number, param1: number, param2: globalAndroid.view.KeyEvent): boolean;
					public constructor(param0: globalAndroid.content.Context);
					public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.util.AttributeSet);
					public unscheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable, param1: java.lang.Runnable): void;
					public onKeyLongPress(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public onKeyUp(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public unscheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable): void;
					public invalidateDrawable(param0: globalAndroid.graphics.drawable.Drawable): void;
					public scheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable, param1: java.lang.Runnable, param2: number): void;
					public sendAccessibilityEvent(param0: number): void;
				}
			}
		}
	}
}

declare module com {
	export module telerik {
		export module widget {
			export module indicators {
				export class GaugeRadialNeedle extends com.telerik.widget.indicators.GaugeIndicator {
					public static class: java.lang.Class<com.telerik.widget.indicators.GaugeRadialNeedle>;
					public drawValue: number;
					public getCircleRadius(): number;
					public getCircleFillPaint(): globalAndroid.graphics.Paint;
					public setTopWidth(param0: number): void;
					public setCircleRadius(param0: number): void;
					public getLength(): number;
					public getOffset(): number;
					public getBottomWidth(): number;
					public getCircleInnerRadius(): number;
					public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.util.AttributeSet);
					public getValue(): number;
					public startAnimation(param0: globalAndroid.view.animation.Animation): void;
					public setOffset(param0: number): void;
					public getAnimationStartValue(): number;
					public onKeyUp(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public unscheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable): void;
					public setCircleStrokeColor(param0: number): void;
					public sendAccessibilityEvent(param0: number): void;
					public setCircleStrokeWidth(param0: number): void;
					public onKeyDown(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public onDraw(param0: globalAndroid.graphics.Canvas): void;
					public getCircleStrokeColor(): number;
					public setupValueAnimator(): void;
					public getTopWidth(): number;
					public startAnimation(): void;
					public getCircleStrokePaint(): globalAndroid.graphics.Paint;
					public initWithXml(param0: globalAndroid.content.res.TypedArray): void;
					public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.util.AttributeSet, param2: number);
					public init(): void;
					public getCircleFillColor(): number;
					public sendAccessibilityEventUnchecked(param0: globalAndroid.view.accessibility.AccessibilityEvent): void;
					public onKeyMultiple(param0: number, param1: number, param2: globalAndroid.view.KeyEvent): boolean;
					public setLength(param0: number): void;
					public constructor(param0: globalAndroid.content.Context);
					public setCircleFillColor(param0: number): void;
					public unscheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable, param1: java.lang.Runnable): void;
					public setValue(param0: number): void;
					public onRestoreInstanceState(param0: globalAndroid.os.Parcelable): void;
					public onKeyLongPress(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public getCircleStrokeWidth(): number;
					public setBottomWidth(param0: number): void;
					public setAnimationStartValue(param0: number): void;
					public onSaveInstanceState(): globalAndroid.os.Parcelable;
					public setCircleFillPaint(param0: globalAndroid.graphics.Paint): void;
					public setCircleStrokePaint(param0: globalAndroid.graphics.Paint): void;
					public invalidateDrawable(param0: globalAndroid.graphics.drawable.Drawable): void;
					public scheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable, param1: java.lang.Runnable, param2: number): void;
					public setCircleInnerRadius(param0: number): void;
				}
				export module GaugeRadialNeedle {
					export class SavedState extends globalAndroid.view.View.BaseSavedState {
						public static class: java.lang.Class<com.telerik.widget.indicators.GaugeRadialNeedle.SavedState>;
						public static CREATOR: globalAndroid.os.Parcelable.Creator<com.telerik.widget.indicators.GaugeRadialNeedle.SavedState>;
						public describeContents(): number;
						public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
					}
				}
			}
		}
	}
}

declare module com {
	export module telerik {
		export module widget {
			export module scales {
				export class GaugeRadialScale extends com.telerik.widget.scales.GaugeScale {
					public static class: java.lang.Class<com.telerik.widget.scales.GaugeRadialScale>;
					public invalidateChildInParent(param0: native.Array<number>, param1: globalAndroid.graphics.Rect): globalAndroid.view.ViewParent;
					public focusSearch(param0: number): globalAndroid.view.View;
					public focusableViewAvailable(param0: globalAndroid.view.View): void;
					public createContextMenu(param0: globalAndroid.view.ContextMenu): void;
					public isLayoutRequested(): boolean;
					public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.util.AttributeSet);
					public addView(param0: globalAndroid.view.View, param1: number, param2: number): void;
					public onKeyUp(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public addView(param0: globalAndroid.view.View): void;
					public addView(param0: globalAndroid.view.View, param1: number, param2: globalAndroid.view.ViewGroup.LayoutParams): void;
					public focusSearch(param0: globalAndroid.view.View, param1: number): globalAndroid.view.View;
					public setSweepAngle(param0: number): void;
					public recomputeViewAttributes(param0: globalAndroid.view.View): void;
					public clearChildFocus(param0: globalAndroid.view.View): void;
					public getOnScreenValue(param0: number): number;
					public onDraw(param0: globalAndroid.graphics.Canvas): void;
					public initWithXml(param0: globalAndroid.content.res.TypedArray): void;
					public getChildVisibleRect(param0: globalAndroid.view.View, param1: globalAndroid.graphics.Rect, param2: globalAndroid.graphics.Point): boolean;
					public getRadius(): number;
					public bringChildToFront(param0: globalAndroid.view.View): void;
					public unscheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable, param1: java.lang.Runnable): void;
					public requestTransparentRegion(param0: globalAndroid.view.View): void;
					public onKeyLongPress(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public childDrawableStateChanged(param0: globalAndroid.view.View): void;
					public scheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable, param1: java.lang.Runnable, param2: number): void;
					public requestFitSystemWindows(): void;
					public getParent(): globalAndroid.view.ViewParent;
					public startActionModeForChild(param0: globalAndroid.view.View, param1: globalAndroid.view.ActionMode.Callback): globalAndroid.view.ActionMode;
					public getSweepAngle(): number;
					public updateViewLayout(param0: globalAndroid.view.View, param1: globalAndroid.view.ViewGroup.LayoutParams): void;
					public requestChildFocus(param0: globalAndroid.view.View, param1: globalAndroid.view.View): void;
					public requestDisallowInterceptTouchEvent(param0: boolean): void;
					public setStartAngle(param0: number): void;
					public addView(param0: globalAndroid.view.View, param1: number): void;
					public unscheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable): void;
					public addView(param0: globalAndroid.view.View, param1: globalAndroid.view.ViewGroup.LayoutParams): void;
					public invalidateChild(param0: globalAndroid.view.View, param1: globalAndroid.graphics.Rect): void;
					public sendAccessibilityEvent(param0: number): void;
					public requestLayout(): void;
					public requestSendAccessibilityEvent(param0: globalAndroid.view.View, param1: globalAndroid.view.accessibility.AccessibilityEvent): boolean;
					public getParentForAccessibility(): globalAndroid.view.ViewParent;
					public onKeyDown(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public getLocation(param0: number): number;
					public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.util.AttributeSet, param2: number);
					public init(): void;
					public showContextMenuForChild(param0: globalAndroid.view.View): boolean;
					public sendAccessibilityEventUnchecked(param0: globalAndroid.view.accessibility.AccessibilityEvent): void;
					public onKeyMultiple(param0: number, param1: number, param2: globalAndroid.view.KeyEvent): boolean;
					public removeView(param0: globalAndroid.view.View): void;
					public constructor(param0: globalAndroid.content.Context);
					public getStartAngle(): number;
					public setRadius(param0: number): void;
					public invalidateDrawable(param0: globalAndroid.graphics.drawable.Drawable): void;
					public requestChildRectangleOnScreen(param0: globalAndroid.view.View, param1: globalAndroid.graphics.Rect, param2: boolean): boolean;
				}
			}
		}
	}
}

declare module com {
	export module telerik {
		export module widget {
			export module scales {
				export abstract class GaugeScale extends globalAndroid.view.ViewGroup {
					public static class: java.lang.Class<com.telerik.widget.scales.GaugeScale>;
					public owner: com.telerik.widget.gauge.RadGaugeView;
					public indicators: java.util.ArrayList<com.telerik.widget.indicators.GaugeIndicator>;
					public maximum: number;
					public minimum: number;
					public lineVisible: boolean;
					public fillPaint: globalAndroid.graphics.Paint;
					public fillColor: number;
					public strokePaint: globalAndroid.graphics.Paint;
					public strokeColor: number;
					public strokeWidth: number;
					public majorTicksFillPaint: globalAndroid.graphics.Paint;
					public majorTicksStrokePaint: globalAndroid.graphics.Paint;
					public majorTicksFillColor: number;
					public majorTicksStrokeColor: number;
					public minorTicksFillPaint: globalAndroid.graphics.Paint;
					public minorTicksStrokePaint: globalAndroid.graphics.Paint;
					public minorTicksFillColor: number;
					public minorTicksStrokeColor: number;
					public labelsPaint: globalAndroid.graphics.Paint;
					public labelsColor: number;
					public majorTicksCount: number;
					public minorTicksCount: number;
					public ticksVisible: boolean;
					public majorTicksLength: number;
					public minorTicksLength: number;
					public majorTicksWidth: number;
					public minorTicksWidth: number;
					public ticksOffset: number;
					public ticksLayoutMode: com.telerik.widget.scales.GaugeScaleTicksLayoutMode;
					public labelsVisible: boolean;
					public labelsCount: number;
					public labelsLayoutMode: com.telerik.widget.scales.GaugeScaleLabelsLayoutMode;
					public labelsOffset: number;
					public labelsFormat: string;
					public getMajorTicksWidth(): number;
					public invalidateChildInParent(param0: native.Array<number>, param1: globalAndroid.graphics.Rect): globalAndroid.view.ViewParent;
					public setMinorTicksWidth(param0: number): void;
					public getLabelsColor(): number;
					public createContextMenu(param0: globalAndroid.view.ContextMenu): void;
					public getLabelsPaint(): globalAndroid.graphics.Paint;
					public removeAllIndicators(): void;
					public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.util.AttributeSet);
					public setMinorTicksLength(param0: number): void;
					public getMajorTicksLength(): number;
					public removeIndicator(param0: number): void;
					public onKeyUp(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public setStrokePaint(param0: globalAndroid.graphics.Paint): void;
					public addView(param0: globalAndroid.view.View): void;
					public addView(param0: globalAndroid.view.View, param1: number, param2: globalAndroid.view.ViewGroup.LayoutParams): void;
					public focusSearch(param0: globalAndroid.view.View, param1: number): globalAndroid.view.View;
					public getMinorTicksWidth(): number;
					public setMajorTicksFillPaint(param0: globalAndroid.graphics.Paint): void;
					public clearChildFocus(param0: globalAndroid.view.View): void;
					public getStrokeWidth(): number;
					public isLabelsVisible(): boolean;
					public addIndicator(param0: number, param1: com.telerik.widget.indicators.GaugeIndicator): void;
					public setTicksOffset(param0: number): void;
					public getMajorTicksStrokeColor(): number;
					public getMajorTicksFillColor(): number;
					public getIndicators(): java.util.ArrayList<com.telerik.widget.indicators.GaugeIndicator>;
					public getChildVisibleRect(param0: globalAndroid.view.View, param1: globalAndroid.graphics.Rect, param2: globalAndroid.graphics.Point): boolean;
					public setOwner(param0: com.telerik.widget.gauge.RadGaugeView): void;
					public getLabelsOffset(): number;
					public setLabelsFormat(param0: string): void;
					public requestTransparentRegion(param0: globalAndroid.view.View): void;
					public getTicksLayoutMode(): com.telerik.widget.scales.GaugeScaleTicksLayoutMode;
					public setLineVisible(param0: boolean): void;
					public getMajorTicksStrokePaint(): globalAndroid.graphics.Paint;
					public setLabelsOffset(param0: number): void;
					public getParent(): globalAndroid.view.ViewParent;
					public isTicksVisible(): boolean;
					public getFillPaint(): globalAndroid.graphics.Paint;
					public getOwner(): com.telerik.widget.gauge.RadGaugeView;
					public requestArrange(): void;
					public getMajorTicksFillPaint(): globalAndroid.graphics.Paint;
					public getLabelsCount(): number;
					public unscheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable): void;
					public sendAccessibilityEvent(param0: number): void;
					public addIndicator(param0: com.telerik.widget.indicators.GaugeIndicator): void;
					public requestLayout(): void;
					public getLabelsFormat(): string;
					public isLineVisible(): boolean;
					public getLocation(param0: number): number;
					public sendAccessibilityEventUnchecked(param0: globalAndroid.view.accessibility.AccessibilityEvent): void;
					public setMinorTicksCount(param0: number): void;
					public setMaximum(param0: number): void;
					public constructor(param0: globalAndroid.content.Context);
					public setStrokeColor(param0: number): void;
					public setFillColor(param0: number): void;
					public requestChildRectangleOnScreen(param0: globalAndroid.view.View, param1: globalAndroid.graphics.Rect, param2: boolean): boolean;
					public focusSearch(param0: number): globalAndroid.view.View;
					public setFillPaint(param0: globalAndroid.graphics.Paint): void;
					public focusableViewAvailable(param0: globalAndroid.view.View): void;
					public setMinorTicksStrokeColor(param0: number): void;
					public isLayoutRequested(): boolean;
					public setIndicators(param0: java.util.ArrayList<com.telerik.widget.indicators.GaugeIndicator>): void;
					public setLabelsCount(param0: number): void;
					public getMinorTicksStrokeColor(): number;
					public addView(param0: globalAndroid.view.View, param1: number, param2: number): void;
					public setTicksVisible(param0: boolean): void;
					public recomputeViewAttributes(param0: globalAndroid.view.View): void;
					public getOnScreenValue(param0: number): number;
					public onDraw(param0: globalAndroid.graphics.Canvas): void;
					public getMajorTicksCount(): number;
					public setLabelsPaint(param0: globalAndroid.graphics.Paint): void;
					public initWithXml(param0: globalAndroid.content.res.TypedArray): void;
					public getMinorTicksCount(): number;
					public setMajorTicksCount(param0: number): void;
					public bringChildToFront(param0: globalAndroid.view.View): void;
					public unscheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable, param1: java.lang.Runnable): void;
					public setMajorTicksWidth(param0: number): void;
					public setLabelsLayoutMode(param0: com.telerik.widget.scales.GaugeScaleLabelsLayoutMode): void;
					public onKeyLongPress(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public childDrawableStateChanged(param0: globalAndroid.view.View): void;
					public scheduleDrawable(param0: globalAndroid.graphics.drawable.Drawable, param1: java.lang.Runnable, param2: number): void;
					public requestFitSystemWindows(): void;
					public setStrokeWidth(param0: number): void;
					public startActionModeForChild(param0: globalAndroid.view.View, param1: globalAndroid.view.ActionMode.Callback): globalAndroid.view.ActionMode;
					public setMinorTicksStrokePaint(param0: globalAndroid.graphics.Paint): void;
					public setRange(param0: number, param1: number): void;
					public setMajorTicksFillColor(param0: number): void;
					public setMajorTicksStrokePaint(param0: globalAndroid.graphics.Paint): void;
					public setMajorTicksLength(param0: number): void;
					public setLabelsVisible(param0: boolean): void;
					public updateViewLayout(param0: globalAndroid.view.View, param1: globalAndroid.view.ViewGroup.LayoutParams): void;
					public requestChildFocus(param0: globalAndroid.view.View, param1: globalAndroid.view.View): void;
					public setLabelsColor(param0: number): void;
					public setTicksLayoutMode(param0: com.telerik.widget.scales.GaugeScaleTicksLayoutMode): void;
					public requestDisallowInterceptTouchEvent(param0: boolean): void;
					public addView(param0: globalAndroid.view.View, param1: number): void;
					public getMinorTicksFillPaint(): globalAndroid.graphics.Paint;
					public setMinorTicksFillPaint(param0: globalAndroid.graphics.Paint): void;
					public addView(param0: globalAndroid.view.View, param1: globalAndroid.view.ViewGroup.LayoutParams): void;
					public invalidateChild(param0: globalAndroid.view.View, param1: globalAndroid.graphics.Rect): void;
					public getMinorTicksLength(): number;
					public requestSendAccessibilityEvent(param0: globalAndroid.view.View, param1: globalAndroid.view.accessibility.AccessibilityEvent): boolean;
					public getParentForAccessibility(): globalAndroid.view.ViewParent;
					public setMajorTicksStrokeColor(param0: number): void;
					public onKeyDown(param0: number, param1: globalAndroid.view.KeyEvent): boolean;
					public getTicksOffset(): number;
					public getMinimum(): number;
					public setMinimum(param0: number): void;
					public removeIndicator(param0: com.telerik.widget.indicators.GaugeIndicator): void;
					public requestRender(): void;
					public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.util.AttributeSet, param2: number);
					public init(): void;
					public showContextMenuForChild(param0: globalAndroid.view.View): boolean;
					public onKeyMultiple(param0: number, param1: number, param2: globalAndroid.view.KeyEvent): boolean;
					public removeView(param0: globalAndroid.view.View): void;
					public getMinorTicksFillColor(): number;
					public getMaximum(): number;
					public animateIndicators(): void;
					public onLayout(param0: boolean, param1: number, param2: number, param3: number, param4: number): void;
					public getMinorTicksStrokePaint(): globalAndroid.graphics.Paint;
					public setMinorTicksFillColor(param0: number): void;
					public invalidateDrawable(param0: globalAndroid.graphics.drawable.Drawable): void;
					public getLabelsLayoutMode(): com.telerik.widget.scales.GaugeScaleLabelsLayoutMode;
					public getFillColor(): number;
					public getStrokeColor(): number;
					public getStrokePaint(): globalAndroid.graphics.Paint;
				}
			}
		}
	}
}

declare module com {
	export module telerik {
		export module widget {
			export module scales {
				export class GaugeScaleLabelsLayoutMode {
					public static class: java.lang.Class<com.telerik.widget.scales.GaugeScaleLabelsLayoutMode>;
					public static OUTER: com.telerik.widget.scales.GaugeScaleLabelsLayoutMode;
					public static INNER: com.telerik.widget.scales.GaugeScaleLabelsLayoutMode;
					public static values(): native.Array<com.telerik.widget.scales.GaugeScaleLabelsLayoutMode>;
					public static valueOf(param0: string): com.telerik.widget.scales.GaugeScaleLabelsLayoutMode;
					public static valueOf(param0: java.lang.Class<any>, param1: string): java.lang.Enum<any>;
				}
			}
		}
	}
}

declare module com {
	export module telerik {
		export module widget {
			export module scales {
				export class GaugeScaleTicksLayoutMode {
					public static class: java.lang.Class<com.telerik.widget.scales.GaugeScaleTicksLayoutMode>;
					public static OUTER: com.telerik.widget.scales.GaugeScaleTicksLayoutMode;
					public static INNER: com.telerik.widget.scales.GaugeScaleTicksLayoutMode;
					public static valueOf(param0: string): com.telerik.widget.scales.GaugeScaleTicksLayoutMode;
					public static values(): native.Array<com.telerik.widget.scales.GaugeScaleTicksLayoutMode>;
					public static valueOf(param0: java.lang.Class<any>, param1: string): java.lang.Enum<any>;
				}
			}
		}
	}
}

//Generics information: