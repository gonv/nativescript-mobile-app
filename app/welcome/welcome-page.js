var frameModule = require("tns-core-modules/ui/frame");
var WelcomeViewModel = require("./welcome-view-model");
var welcomeViewModel = new WelcomeViewModel();
var app = require("tns-core-modules/application");
var platform = require("platform");
var color = require("tns-core-modules/color");

function pageLoaded(args) {
	var page = args.object;
	if (app.ios) {
		frameModule.topmost().ios.controller.navigationBar.barStyle = 1;
	}

	if (app.android && platform.device.sdkVersion >= '21') {
		var View = android.view.View;
		const window = app.android.foregroundActivity.getWindow();
		window.setStatusBarColor(new color.Color("#25325c").android);
	}

	page.bindingContext = welcomeViewModel;
}

exports.pageLoaded = pageLoaded;
