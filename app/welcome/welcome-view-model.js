var observableModule = require("tns-core-modules/data/observable");
const topmost = require("ui/frame").topmost;
const Observable = require("tns-core-modules/data/observable").Observable;
var viewModel = new Observable();

function WelcomeViewModel() {

	viewModel.page_change = function () {
		topmost().navigate("./home-page/home");
	}

	return viewModel;
}

module.exports = WelcomeViewModel;
